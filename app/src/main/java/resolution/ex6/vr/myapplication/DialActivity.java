package resolution.ex6.vr.myapplication;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class DialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dial);

        Intent intent = this.getIntent();

        TextView nameText = (TextView) findViewById(R.id.textView4);
        TextView phoneText = (TextView) findViewById(R.id.textView5);

        nameText.setText(nameText.getText() + intent.getStringExtra(MainActivity.NAME_STRING));
        phoneText.setText(phoneText.getText() + intent.getStringExtra(MainActivity.PHONE_STRING));
    }

    public void onClickDial(View v) {
        TextView phoneText = (TextView) findViewById(R.id.textView5);
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneText.getText()));
        startActivity(intent);
    }
}
