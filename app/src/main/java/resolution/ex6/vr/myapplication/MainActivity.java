package resolution.ex6.vr.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    static final String NAME_STRING = "NAME";
    static final String PHONE_STRING = "PHONE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickComplete(View v) {
        Intent intent = new Intent(MainActivity.this, DialActivity.class);

        EditText nameEdit = (EditText) findViewById(R.id.editText);
        EditText phoneEdit = (EditText) findViewById(R.id.editText2);

        intent.putExtra(NAME_STRING, nameEdit.getText().toString());
        intent.putExtra(PHONE_STRING, phoneEdit.getText().toString());

        startActivity(intent);

        finish();
    }
}
